package com.example.bottomnavigation;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class MainActivity extends AppCompatActivity {
    EditText username,password;
    Systeminterface systeminterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        username = findViewById(R.id.username);
        password = findViewById(R.id.password);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Systeminterface.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        Systeminterface systeminterface = retrofit.create(Systeminterface.class);

        if (username.getText().toString().equals("admin")) {
            Intent intent = new Intent(MainActivity.this, Register.class);
            startActivity(intent);
        }
    }
    public void onClickhome (View view){
        Intent intent = new Intent(MainActivity.this, HomeFragment.class);
        startActivity(intent);

        Call<List<login>> responseBodyCall = systeminterface.login(username.getText().toString(),
                password.getText().toString());
        responseBodyCall.enqueue(new Callback<List<login>>() {
            @Override
            public void onResponse(Call<List<login>> call, Response<List<login>> response) {

            }

            @Override
            public void onFailure(Call<List<login>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "" + t, Toast.LENGTH_SHORT).show();
            }
        });

    }

    public void onClickregister (View view){
        Button button = (Button) findViewById(R.id.btn7);
        Intent intent = new Intent(MainActivity.this, Register.class);
        startActivity(intent);
    }

    public void onClickback (View view){
        Button button = (Button) findViewById(R.id.btn10);
        Intent intent = new Intent(MainActivity.this, HomeFragment.class);
        startActivity(intent);
    }
}