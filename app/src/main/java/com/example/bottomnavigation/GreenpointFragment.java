package com.example.bottomnavigation;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import androidx.fragment.app.Fragment;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link GiftshopFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class GreenpointFragment extends Fragment {
    ListView listView;
    listshop listshop;
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private Activity view;



    public GreenpointFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment GiftFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static HomeFragment newInstance(String param1, String param2) {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_home, container,
                false);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Systeminterface.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        Systeminterface systeminterface = retrofit.create(Systeminterface.class);

        Call<List<shopresponse>> shopcall = systeminterface.getshop();
        shopcall.enqueue(new Callback<List<shopresponse>>() {
            @Override
            public void onResponse(Call<List<shopresponse>> call, final Response<List<shopresponse>> response) {


                listView = view.findViewById(R.id.listshop);
                listshop = new listshop(getContext(),response.body());
                listView.setAdapter(listshop);

                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        String[] images = {

                                response.body().get(position).getShopName(),
                                response.body().get(position).getUsername(),
                                response.body().get(position).getPhone(),
                                response.body().get(position).getAddress(),

                        };
                        Intent intent = new Intent(getContext(), detailshop.class);
                        intent.putExtra("getShopName",images[0]);
                        intent.putExtra("getUsername",images[1]);
                        intent.putExtra("getPhone",images[2]);
                        intent.putExtra("getAddress",images[3]);
                        startActivity(intent);
                    }
                });
            }

            @Override
            public void onFailure(Call<List<shopresponse>> call, Throwable t) {

            }

        });

        return view;

        }
    }

