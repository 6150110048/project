package com.example.bottomnavigation;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

public class listshop extends BaseAdapter {
    private Context context;
    private List<shopresponse> listshop;
    private LayoutInflater layoutInflater;

    public listshop(Context applicationContext, List<shopresponse> listshop) {
        context = applicationContext;
        this.listshop = listshop;

    }

    @Override
    public int getCount() {
        return listshop.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (layoutInflater == null){
            layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        if (view == null){
            view = layoutInflater.inflate(R.layout.activity_listshop,null);
        }

        TextView shopname2 = view.findViewById(R.id.nameshop);
        TextView add_shop2 = view.findViewById(R.id.addresshop);

        shopresponse getlistshop = listshop.get(i);

        shopname2.setText(getlistshop.getShopName());
        add_shop2.setText(getlistshop.getAddress());

        return view;
    }
}




