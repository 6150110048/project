package com.example.bottomnavigation;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class detailshop extends AppCompatActivity {
    TextView getshop_name, getusername, getPhone,getaddress;
    ImageView imageView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detailshop);
        setTitle("ข้อมูลร้านค้า");

        getshop_name = findViewById(R.id.getshop_name);
        getusername = findViewById(R.id.getusername);
        getPhone = findViewById(R.id.getPhone);
        getaddress = findViewById(R.id.getaddress);

        Intent i=this.getIntent();
        String id=i.getExtras().getString("getshop_name");
        String path=i.getExtras().getString("getusername");
        String desc=i.getExtras().getString("getPhone");
        String address2=i.getExtras().getString("getadderss");
        getshop_name.setText(id);
        getusername.setText(desc);
        getPhone.setText(path);
        getaddress.setText(address2);
    }
}


