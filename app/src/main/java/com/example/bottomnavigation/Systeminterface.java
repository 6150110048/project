package com.example.bottomnavigation;


import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface Systeminterface {
    public static String BASE_URL = "https://itlearningcenters.com/android/project0105/";


    @FormUrlEncoded
    @POST("regist_cust.php")
    Call<ResponseBody> register(@Field("customer_name") String nameValue,
                                @Field("email") String emailValue,
                                @Field("phone_number") String phoneNoValue,
                                @Field("username") String userNameValue,
                                @Field("password") String passWordValue,
                                @Field("address") String addressValue

    );


    @FormUrlEncoded
    @POST("cust_login.php")
    Call<List<login>> login(@Field("username") String userNameValue,
                            @Field("password") String passWordValue
    );


    @GET("cust_profile.php")
    Call<customerresponse> cust_profile(@Query("cust_id") String cust_id);

    @GET("list_shops.php")
    Call<List<shopresponse>> getshop();



}