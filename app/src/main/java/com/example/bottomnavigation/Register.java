package com.example.bottomnavigation;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class Register extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        final EditText username = findViewById(R.id.editTextTextPersonName5);
        final EditText password = findViewById(R.id.editTextTextPersonName2);
        final EditText name = findViewById(R.id.editTextTextPersonName);

        final EditText phonenumber = findViewById(R.id.editTextTextPersonName3);
        final EditText email = findViewById(R.id.editTextTextPersonName4);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Systeminterface.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        final Systeminterface systeminterface = retrofit.create(Systeminterface.class);

        Button next = (Button) findViewById(R.id.btn5);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Call<ResponseBody> responseBodyCall = systeminterface.register(
                        name.getText().toString(),email.getText().toString(),

                        phonenumber.getText().toString(),
                        username.getText().toString(),
                        password.getText().toString(),
                        "");
responseBodyCall.enqueue(new Callback<ResponseBody>() {
    @Override
    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

    }

    @Override
    public void onFailure(Call<ResponseBody> call, Throwable t) {
        Toast.makeText(getApplicationContext(), "" + t, Toast.LENGTH_SHORT).show();
    }
});
                Intent i = new Intent(Register.this, MainActivity.class);
                startActivity(i);
            }
        });
        Button next1 = (Button) findViewById(R.id.btn4);
        next1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Register.this, MainActivity.class);
                startActivity(i);
            }
        });

    }
}