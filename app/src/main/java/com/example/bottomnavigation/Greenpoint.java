package com.example.bottomnavigation;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;


import android.os.Bundle;
import android.view.MenuItem;

import com.google.android.material.bottomnavigation.BottomNavigationView;

public class Greenpoint extends AppCompatActivity {

    BottomNavigationView bottomNavigation;
    private ActionBar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.greenpoint);
        toolbar = getSupportActionBar();
        bottomNavigation = findViewById(R.id.bottom_navigation);
        bottomNavigation.setOnNavigationItemSelectedListener(new
                                                                     BottomNavigationView.OnNavigationItemSelectedListener() {
                                                                         @Override
                                                                         public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                                                                             switch (item.getItemId()) {
                                                                                 case R.id.navigation_home:
                                                                                     toolbar.setTitle("หน้าแรก");
                                                                                     openFragment(HomeFragment.newInstance("HOME", ""));
                                                                                     return true;
                                                                                 case R.id.navigation_card:
                                                                                     toolbar.setTitle("บัตรสะสมแต้ม");
                                                                                     openFragment(CardFragment.newInstance("Card", ""));
                                                                                     return true;
                                                                                 case R.id.navigation_list:
                                                                                     toolbar.setTitle("รายการ");
                                                                                     openFragment(ListFragment.newInstance("List", ""));
                                                                                     return true;
                                                                                 case R.id.navigation_gift:
                                                                                     toolbar.setTitle("แลกของขวัญ");
                                                                                     openFragment(GiftFragment.newInstance("Gift", ""));
                                                                                     return true;

                                                                             }
                                                                             return false;
                                                                         }
                                                                     });
    }
    public void openFragment(Fragment fragment) {
        FragmentTransaction transaction =
                getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }
}