package com.example.bottomnavigation;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Giftsshop extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_giftsshop);

        Button button11 = findViewById(R.id.btn1);
        button11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Giftsshop.this,Success.class);
                startActivity(i);
            }
        });

        Button button22 = findViewById(R.id.btn2);
        button22.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Giftsshop.this,Success.class);
                startActivity(i);
            }
        });

        Button button33 = findViewById(R.id.btn3);
        button33.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Giftsshop.this,Success.class);
                startActivity(i);
            }
        });
    }
}